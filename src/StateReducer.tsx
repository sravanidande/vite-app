import { Switch, Button, HStack, Container } from '@chakra-ui/react'
import React from 'react'

interface ToggleState {
  readonly on: boolean
  readonly toggleCount: number
}

function toggleReducer(state: ToggleState, action: any) {
  switch (action.type) {
    case 'Toggle': {
      return { on: !state.on, toggleCount: state.toggleCount + 1 }
    }
    case 'ON': {
      return { ...state, on: true }
    }
    case 'OFF': {
      return { ...state, on: false }
    }
    case 'Reset': {
      return { ...state, toggleCount: 0 }
    }
    default: {
      return state
    }
  }
}

export function useToggle() {
  const [state, dispatch] = React.useReducer(toggleReducer, {
    on: true,
    toggleCount: 0,
  })
  const switchOff = () => dispatch({ type: 'OFF' })
  const switchON = () => dispatch({ type: 'ON' })
  const toggle = () => dispatch({ type: 'Toggle' })
  const reset = () => dispatch({ type: 'Reset' })
  return { state, switchON, switchOff, toggle, reset }
}

export const ToggleSwitch = () => {
  const { state, switchON, switchOff, toggle, reset } = useToggle()

  return (
    <Container>
      <HStack>
        <Button onClick={switchOff}>Switch off</Button>
        <Button onClick={switchON}>Switch on</Button>
      </HStack>
      <Switch
        size="lg"
        mt="8px"
        isChecked={state.on}
        onChange={toggle}
        isReadOnly={state.toggleCount >= 4}
      />
      {state.toggleCount >= 4 ? (
        <Button onClick={reset} m={2}>
          Reset
        </Button>
      ) : null}
    </Container>
  )
}
