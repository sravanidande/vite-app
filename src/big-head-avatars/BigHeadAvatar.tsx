import { BigHead } from '@bigheads/core'
import { Box, Heading, HStack } from '@chakra-ui/layout'
import React from 'react'

interface WindowSize {
  readonly width: number
  readonly height: number
}

export const useWindowSize = () => {
  const [windowSize, setWindowSize] = React.useState<WindowSize | undefined>(
    undefined,
  )

  React.useEffect(() => {
    const handleResize = () => {
      setWindowSize({ width: window.innerWidth, height: window.innerHeight })
    }
    window.addEventListener('resize', handleResize)
    handleResize()
    return () => window.removeEventListener('resize', handleResize)
  }, [])

  return windowSize
}

interface AvatarDetailsProps {
  readonly name: string
  readonly size: string | number
}

export const AvatarDetails: React.FC<AvatarDetailsProps> = ({ name, size }) => {
  return (
    <p>
      you are seeing{' '}
      <span style={{ fontWeight: 'bold', fontSize: '20px' }}>{name}</span>{' '}
      because your screen is 😃
      <span style={{ fontWeight: 'bold', fontSize: '20px' }}>{size}</span>
    </p>
  )
}

export const BigHeadAvatar: React.FC = () => {
  const windowSize = useWindowSize()
  return (
    <Box maxH="150px" maxW="450px">
      <HStack>
        <Heading as="h4">
          {windowSize?.width}/{windowSize?.height}
        </Heading>
        <BigHead
          clothingColor="black"
          eyebrows="angry"
          eyes="wink"
          facialHair="mediumBeard"
          graphic="vue"
          hair="short"
          hairColor="black"
          hat="none"
          hatColor="green"
          lipColor="purple"
          mouth="open"
          skinTone="brown"
        />
        <>
          {windowSize && windowSize.width < 700 ? (
            <AvatarDetails name="mithi" size="small" />
          ) : (
            <>
              {windowSize && windowSize?.width > 1000 ? (
                <AvatarDetails name="chris" size="big" />
              ) : (
                <AvatarDetails name="kent" size="medium" />
              )}
            </>
          )}
        </>
      </HStack>
    </Box>
  )
}
