import { Flex, Link as CLink, Text } from '@chakra-ui/react'
import React from 'react'
import { supabase } from '../api/supabaseClient'

export const SignoutPage: React.FC = () => {
  React.useEffect(() => {
    supabase.auth.signOut()
  }, [])

  return (
    <Flex h="100vh" align="center" justify="center" direction="column">
      <Text>You have been Signed Out</Text>
      <Text color="blue.300">
        Go to{' '}
        <CLink textDecoration="underline" href="/">
          Sign in
        </CLink>{' '}
        page
      </Text>
    </Flex>
  )
}
