import {
  Button,
  Flex,
  FormControl,
  Heading,
  Input,
  Text,
  VStack,
} from '@chakra-ui/react'
import React from 'react'
import { useHistory } from 'react-router-dom'
import { supabase } from '../api/supabaseClient'
import { useAuth } from './AuthContext'
import { Profile } from './ProfileView'
import { UploadAvatar } from './UploadAvatar'

export const CreateProfilePage: React.FC = () => {
  const session = useAuth()
  const [profile, setProfile] = React.useState<Profile>({
    id: session?.session?.user?.id,
    username: '',
    website: '',
    avatar_url: '',
  })
  const [error, setError] = React.useState<string | undefined>(undefined)
  const history = useHistory()

  const handleProfileSubmit = async (profile: Profile) => {
    const { error } = await supabase
      .from('profiles')
      .insert([profile], { returning: 'minimal' })
    if (error) {
      setError(error.message)
    }

    history.push('/home')
  }

  return (
    <Flex
      h="90vh"
      align="center"
      justify="center"
      direction="column"
      maxW="700px"
    >
      <Heading mb="20px">Create Your Profile</Heading>
      <form onSubmit={evt => evt.preventDefault()}>
        <VStack>
          <UploadAvatar
            avatar={profile.avatar_url}
            onChange={avatar => setProfile({ ...profile, avatar_url: avatar })}
          />
          <FormControl>
            <Input
              type="text"
              placeholder="Username"
              value={profile.username}
              onChange={evt =>
                setProfile({ ...profile, username: evt.target.value })
              }
            />
          </FormControl>
          <FormControl>
            <Input
              type="text"
              placeholder="Website"
              value={profile.website}
              onChange={evt =>
                setProfile({ ...profile, website: evt.target.value })
              }
            />
          </FormControl>
        </VStack>
        <Button
          type="submit"
          mt="6"
          onClick={() => handleProfileSubmit(profile)}
        >
          Submit
        </Button>
      </form>
      {error ? <Text>{error}</Text> : null}
    </Flex>
  )
}
