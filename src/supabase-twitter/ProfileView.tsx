import React from 'react'
import { useAuth } from '.'
import { supabase } from '../api/supabaseClient'
import { CreateProfilePage } from './CreatePofilePage'
import { ProfilePageView } from './ProfilePageView'

export interface Profile {
  readonly id?: string
  readonly username: string
  readonly website: string
  readonly avatar_url: string
}
export const ProfileView: React.FC = () => {
  const session = useAuth()
  const [profile, setProfile] = React.useState<Profile | null | undefined>(
    undefined,
  )

  React.useEffect(() => {
    const getData = async () => {
      const loggedUserID = session?.session?.user?.id
      const { data, error } = await supabase
        .from('profiles')
        .select('id,username,avatar_url,website')
        .eq('id', loggedUserID)

      console.log({ error, data })

      if (error) {
        console.log(error)
      }
      if (data) {
        if (data.length === 0) {
          setProfile(null)
        } else {
          setProfile(data[0])
        }
      }
    }
    getData()
  }, [])

  if (profile === undefined) {
    return <h1>Loading...</h1>
  }

  return (
    <>
      {profile !== null ? (
        <ProfilePageView profile={profile} />
      ) : (
        <CreateProfilePage />
      )}
    </>
  )
}
