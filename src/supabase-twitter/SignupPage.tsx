import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  FormLabel,
  Heading,
  HStack,
  Link,
  Input,
  Text,
} from '@chakra-ui/react'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useHistory } from 'react-router-dom'
import { supabase } from '../api/supabaseClient'

export interface User {
  readonly email: string
  readonly password: string
  readonly confirmPassword: string
}

export const SignupPage: React.FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  const history = useHistory()

  const [user, setUser] = React.useState<User>({
    email: '',
    password: '',
    confirmPassword: '',
  })
  const [error, setError] = React.useState('')

  const onUserSubmit = async (user: User) => {
    supabase.auth
      .signUp(user)
      .then(res => {
        if (res.data) {
          setUser(user)

          history.push('/confirm-signup')
        } else if (
          res.error?.message ===
          'A user with this email address has already been registered'
        ) {
          setError('A user with this email address has already been registered')
        }
      })
      .catch(err => console.error(err))
  }

  return (
    <Box w="50%" m="0 auto">
      <Heading mb="6" mt="6">
        Sign up
      </Heading>
      <form onSubmit={evt => evt.preventDefault()}>
        <FormControl isRequired>
          <FormLabel> Email</FormLabel>
          <Input
            type="email"
            placeholder="Enter valid email"
            {...register('email', {
              required: true,
              pattern: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
            })}
            value={user?.email}
            onChange={evt => {
              user && setUser({ ...user, email: evt.target.value })
            }}
          />
          <FormHelperText color="red.300">
            {errors?.password?.type === 'required' && 'This is required field'}
            {errors?.email?.type === 'pattern' && 'Enter valid  email pattern'}
          </FormHelperText>
        </FormControl>

        <FormControl isRequired>
          <FormLabel> Password</FormLabel>
          <Input
            type="password"
            placeholder="Enter password"
            {...register('password', {
              required: true,
              minLength: 8,
            })}
            value={user?.password}
            onChange={evt =>
              user && setUser({ ...user, password: evt.target.value })
            }
          />

          <FormHelperText color="red.300">
            {errors?.password?.type === 'required' && 'This is required field'}
            {errors?.password?.type === 'minLength' &&
              'password should be minimum 8 characters'}
          </FormHelperText>
        </FormControl>

        <FormControl isRequired>
          <FormLabel>Confirm Password</FormLabel>
          <Input
            type="password"
            placeholder="Confirm password"
            {...register('confirmPassword', {
              required: true,
              validate: value =>
                value === user?.password ? undefined : 'password do not match',
            })}
            value={user?.confirmPassword}
            onChange={evt =>
              user && setUser({ ...user, confirmPassword: evt.target.value })
            }
          />
          <FormHelperText color="red.300">
            {errors?.confirmPassword?.message}
            {errors?.password?.type === 'required' && 'This is required field'}
          </FormHelperText>
        </FormControl>

        <HStack mt={6}>
          <Button
            colorScheme="green"
            type="submit"
            onClick={handleSubmit(onUserSubmit)}
          >
            Sign up
          </Button>
          <Button
            colorScheme="green"
            type="submit"
            onClick={() =>
              setUser({ email: '', password: '', confirmPassword: '' })
            }
          >
            Reset
          </Button>
        </HStack>

        <Text mt="6">
          already had an account? go to{' '}
          <Link textDecoration="underline" color="blue.300" href="/">
            Sign in
          </Link>
        </Text>
      </form>
      {error ? <Text color="red.300">{error}</Text> : null}
    </Box>
  )
}
