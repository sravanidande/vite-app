import React from 'react'

interface ProfileContextType {
  readonly name: string
  handleName(name: string): void
}

export const ProfileContext = React.createContext<
  ProfileContextType | undefined
>(undefined)

export const useProfile = () => {
  const context = React.useContext(ProfileContext)
  if (context === undefined) {
    return
  }
  return context
}

export const ProfileContextProvider = ({ children }: any) => {
  const [name, setName] = React.useState<ProfileContextType['name']>('')

  const handleName = (name: string) => {
    setName(name)
  }

  return (
    <ProfileContext.Provider value={{ name, handleName }}>
      {children}
    </ProfileContext.Provider>
  )
}
