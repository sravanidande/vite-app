import { Box, Text } from '@chakra-ui/react'
import React from 'react'
import { useAuth } from './AuthContext'

export const SignupConfirmation: React.FC = () => {
  const session = useAuth()
  return (
    <Box mt="150px">
      <Text>A confirmation mail sent to{session?.session?.user?.email}</Text>
    </Box>
  )
}
