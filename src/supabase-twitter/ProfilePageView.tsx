import { Avatar, Heading, VStack } from '@chakra-ui/react'
import React from 'react'
import { supabase } from '../api/supabaseClient'
import { Profile } from './ProfileView'

interface ProfilePageViewProps {
  readonly profile: Profile
}

export const ProfilePageView: React.FC<ProfilePageViewProps> = ({
  profile,
}) => {
  const [url, setUrl] = React.useState<string | undefined>(undefined)
  React.useEffect(() => {
    const getUrl = async () => {
      const { data: blob, error: newError } = await supabase.storage
        .from('avatars')
        .download(profile.avatar_url)

      if (newError) {
        throw newError
      }

      if (!blob) {
        return
      }
      setUrl(URL.createObjectURL(blob))
    }
    getUrl()
  }, [profile.avatar_url])
  return (
    <VStack mt="60px" p="40px" boxShadow={'0 2px 4px 6px #171923'}>
      <Avatar size="xl" mb="6" bg="grey" src={url} />
      <Heading as="h5">{profile.username}</Heading>
    </VStack>
  )
}
