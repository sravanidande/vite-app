import { Flex, Text } from '@chakra-ui/react'
import React from 'react'

interface HomeProps {
  readonly name: string
}

export const Home: React.FC<HomeProps> = ({ name }) => {
  return (
    <Flex h="100vh" align="center" justify="center" direction="column">
      <Text>Hello {name}, welcome</Text>
    </Flex>
  )
}
