import {
  Box,
  Button,
  FormControl,
  FormHelperText,
  HStack,
  FormLabel,
  Heading,
  Input,
  Link as CLink,
  Text,
} from '@chakra-ui/react'
import React from 'react'
import { useForm } from 'react-hook-form'
import { useHistory } from 'react-router-dom'
import { supabase } from '../api/supabaseClient'
import { User } from './SignupPage'

export const SigninPage: React.FC = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm()

  let history = useHistory()
  const [user, setUser] = React.useState<Partial<User> | undefined>({
    email: '',
    password: '',
  })
  const [error, setError] = React.useState('')

  const onUserSubmit = async (user: Partial<User>): Promise<void> => {
    await supabase.auth
      .signIn(user)
      .then(res => {
        if (res.data) {
          setUser({ email: '', password: '' })
          history.push('/home')
        } else if (res.error?.message === 'Email not confirmed') {
          setError('Please confirm your email before sign in')
        } else if (res.error?.message === 'Invalid email or password') {
          setError('Invalid email or password')
        }
      })
      .catch(err => console.error({ foobat: err }))
  }

  return (
    <Box w="50%" m="0 auto">
      <Heading mb="6" mt="6">
        Sign in
      </Heading>
      <form onSubmit={e => e.preventDefault()}>
        <FormControl isRequired>
          <FormLabel> Email</FormLabel>
          <Input
            type="email"
            placeholder="Enter valid email"
            {...register('email', {
              required: true,
              pattern: /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/,
            })}
            value={user?.email}
            onChange={evt => {
              user && setUser({ ...user, email: evt.target.value })
            }}
          />
          <FormHelperText color="red.300">
            {errors?.password?.type === 'required' && 'This is required field'}
            {errors?.email?.type === 'pattern' && 'Enter valid  email pattern'}
          </FormHelperText>
        </FormControl>

        <FormControl isRequired>
          <FormLabel> Password</FormLabel>
          <Input
            type="password"
            placeholder="Enter password"
            {...register('password', {
              required: true,
            })}
            value={user?.password || ''}
            onChange={evt =>
              user && setUser({ ...user, password: evt.target.value })
            }
          />

          <FormHelperText color="red.300">
            {errors?.password?.type === 'required' && 'This is required field'}
          </FormHelperText>
        </FormControl>

        <HStack mt={8}>
          <Button
            colorScheme="green"
            type="submit"
            onClick={handleSubmit(onUserSubmit)}
          >
            Sign in
          </Button>
          <Button
            colorScheme="green"
            type="submit"
            onClick={() => setUser({ email: '', password: '' })}
          >
            Reset
          </Button>
        </HStack>

        <Text mt="6">
          haven't signed up yet? go to{' '}
          <CLink textDecoration="underline" color="blue.300" href="/signup">
            Sign up
          </CLink>
        </Text>
      </form>
      {error ? <Text color="red.300">{error}</Text> : null}
    </Box>
  )
}
