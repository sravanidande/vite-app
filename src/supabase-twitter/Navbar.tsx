import {
  Avatar,
  Box,
  Button,
  Flex,
  Link as CLink,
  Menu,
  MenuButton,
  useColorModeValue,
} from '@chakra-ui/react'
import React from 'react'
import { Link } from 'react-router-dom'
import { supabase } from '../api/supabaseClient'
import { useAuth } from './AuthContext'

export const Navbar = () => {
  const session = useAuth()
  return (
    <>
      <Box bg={useColorModeValue('gray.100', 'gray.900')} px={4}>
        <Flex h={16} alignItems={'center'} justifyContent={'space-between'}>
          <CLink fontWeight="bold" color="teal.300" href="/home">
            SupabaseTwitter
          </CLink>
          <Flex alignItems={'center'}>
            {session?.session?.user?.id && (
              <Button
                as="a"
                variant={'solid'}
                colorScheme={'teal'}
                size={'sm'}
                mr={4}
                href="/signout"
                onClick={() => {
                  supabase.auth.signOut()
                }}
              >
                Sign out
              </Button>
            )}

            <Menu>
              <MenuButton
                as={Link}
                rounded={'full'}
                cursor={'pointer'}
                to="/create-profile"
              >
                {session?.session?.user?.id && (
                  <Avatar size="sm" bg="teal.500" />
                )}
              </MenuButton>
            </Menu>
          </Flex>
        </Flex>
      </Box>
    </>
  )
}
