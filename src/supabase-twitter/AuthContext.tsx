import { AuthSession } from '@supabase/supabase-js'
import React from 'react'
import { supabase } from '../api/supabaseClient'

interface UserInfo {
  readonly session: AuthSession | null
}

const AuthContext = React.createContext<UserInfo | null>(null)

export function useAuth() {
  const context = React.useContext(AuthContext)
  if (context === null) {
    return
  }
  return context
}

export const AuthProvider: React.FC = ({ children }) => {
  const [session, setSession] = React.useState(() => supabase.auth.session())

  React.useEffect(() => {
    const data = supabase.auth.onAuthStateChange((_, session) => {
      setSession(session)
    })
    return () => {
      data.data?.unsubscribe()
    }
  }, [])

  return (
    <AuthContext.Provider value={{ session }}>{children}</AuthContext.Provider>
  )
}
