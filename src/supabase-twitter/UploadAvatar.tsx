import { FormControl } from '@chakra-ui/form-control'
import { VStack } from '@chakra-ui/layout'
import { Avatar, Button } from '@chakra-ui/react'
import React from 'react'
import { supabase } from '../api/supabaseClient'

interface UploadAvatarProps {
  readonly avatar?: string
  onChange(avatar: string): void
}

export const UploadAvatar: React.FC<UploadAvatarProps> = ({ onChange }) => {
  const [url, set] = React.useState<string | undefined>(undefined)
  const handleChange = async (evt: React.ChangeEvent<HTMLInputElement>) => {
    const files = evt.target.files
    if (files === null || files.length <= 0) {
      return
    }

    const file = files[0]
    const { data, error } = await supabase.storage
      .from('avatars')
      .upload(file.name, file)

    if (error) {
      throw new Error(error.message)
    }
    if (!data) {
      return
    }

    const imgUrl = file.name

    if (imgUrl === undefined) {
      return
    }

    const { data: blob, error: newError } = await supabase.storage
      .from('avatars')
      .download(imgUrl)

    if (newError) {
      throw newError
    }

    if (!blob) {
      return
    }
    set(URL.createObjectURL(blob))
    onChange(imgUrl)
  }

  return (
    <FormControl>
      <VStack>
        <Avatar size="xl" src={url} />
        <Button as="label">
          Upload Avatar
          <input
            type="file"
            onChange={handleChange}
            accept="image/*"
            style={{ visibility: 'hidden', position: 'absolute' }}
          />
        </Button>
      </VStack>
    </FormControl>
  )
}
