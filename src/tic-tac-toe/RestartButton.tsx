import { Button } from '@chakra-ui/react'
import React from 'react'

interface RestartButtonProps {
  onClick(): void
}

export const RestartButton: React.FC<RestartButtonProps> = ({ onClick }) => {
  return (
    <Button
      bgColor="teal.300"
      mt={10}
      fontSize="md"
      fontWeight="bold"
      color="black"
      onClick={onClick}
    >
      Restart
    </Button>
  )
}
