import { Heading } from '@chakra-ui/layout'
import React from 'react'
import { Player } from './Square'

interface PlayerProps {
  readonly player: Player
  readonly winner: Player | 'Game over'
}

export const PlayerStatus: React.FC<PlayerProps> = ({ player, winner }) => {
  return (
    <>
      {winner === 'none' ? (
        <Heading mb={5}>Player {player}, it's your turn!</Heading>
      ) : winner === 'Game over' ? (
        <Heading mb={5}>None! Won </Heading>
      ) : (
        <Heading mb={5}>Winner: Player {winner} 🥳🎉 </Heading>
      )}
    </>
  )
}
