import { Button, Center } from '@chakra-ui/react'
import React from 'react'

export type Player = 'X' | 'O' | 'none'

export interface SquareProps {
  readonly value: Player
  readonly idx: number
  onClick(idx: number): void
}

// export const Value: React.FC<Partial<SquareProps>> = ({ value }) => (
//   <Heading opacity="0.5" fontWeight="extrabold" fontSize="5xl">
//     {value}
//   </Heading>
// )

// export const Dot: React.FC = () => (
//   <Text h={4} w={4} opacity="0.5" bg="teal.500" borderRadius="50%"></Text>
// )

export const SquareView: React.FC<SquareProps> = ({ value, onClick, idx }) => {
  return (
    <Center>
      <Button
        size="lg"
        p={8}
        color="teal"
        borderRadius={20}
        fontWeight="extrabold"
        fontSize="2xl"
        _hover={{ bgColor: 'teal.300', color: 'black' }}
        onClick={() => {
          onClick(idx)
        }}
      >
        {value === 'none' ? '' : value}
      </Button>
    </Center>
  )
}
