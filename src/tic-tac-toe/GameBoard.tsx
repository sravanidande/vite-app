import { Box, Grid, GridItem } from '@chakra-ui/react'
import React from 'react'
import { SquareView, Player } from './Square'

export const n = 3

interface GameBoardProps {
  readonly squares: readonly Player[]
  onClick(idx: number): void
}

export const GameBoard: React.FC<GameBoardProps> = ({ squares, onClick }) => {
  return (
    <Box w="300px" mt="20px">
      <Grid
        templateRows={`repeat(${n},1fr)`}
        templateColumns={`repeat(${n},1fr)`}
        gap={1}
      >
        {squares.map((square, index) => (
          <GridItem key={index}>
            <SquareView onClick={onClick} idx={index} value={square} />
          </GridItem>
        ))}
      </Grid>
    </Box>
  )
}
