import { Button } from '@chakra-ui/button'
import { HStack } from '@chakra-ui/layout'
import { range } from 'lodash'
import React from 'react'

interface StepCountProps {
  readonly count: number
  readonly idx: number
  onClick(idx: number): void
}

export const StepCount: React.FC<StepCountProps> = ({
  count,
  idx,
  onClick,
}) => {
  return (
    <Button
      p={6}
      border="2px solid teal"
      borderRadius={20}
      fontWeight="extrabold"
      fontSize="xl"
      _hover={{ bgColor: 'teal.300', color: 'black' }}
      onClick={() => onClick(idx)}
    >
      {count}
    </Button>
  )
}

interface StepsViewProps {
  readonly stepCount: number
  onClick(idx: number): void
}

export const StepsView: React.FC<StepsViewProps> = ({ stepCount, onClick }) => {
  return (
    <HStack mt={6} p={3}>
      {range(stepCount).map((step, index) => (
        <StepCount key={step} count={step} idx={index} onClick={onClick} />
      ))}
    </HStack>
  )
}
