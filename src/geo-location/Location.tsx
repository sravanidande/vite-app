import { Text } from '@chakra-ui/layout'
import React from 'react'

navigator.geolocation.getCurrentPosition(() => 'helo')

type LocationType = { readonly longitude: number; readonly latitude: number }

interface Location {
  readonly status: 'idle' | 'resolve' | 'reject'
  readonly error?: string
  readonly location?: LocationType
}

const initialLocation: Location = { status: 'idle' }

export function useLocation() {
  const [location, setLocation] = React.useState<Location>(initialLocation)

  React.useEffect(() => {
    const success = (pos: GeolocationPosition) => {
      const coords = pos.coords
      const newLocation: Location['location'] = {
        longitude: coords.latitude,
        latitude: coords.latitude,
      }
      setLocation({ status: 'resolve', location: newLocation })
    }
    const error = (err: GeolocationPositionError) => {
      setLocation({
        status: 'reject',
        error: err.message,
      })
    }
    navigator.geolocation.watchPosition(success, error)
  }, [])

  return location
}

export const Location: React.FC = () => {
  const { location, error, status } = useLocation()

  if (status === 'idle') {
    return <Text>Loading...</Text>
  }

  if (status === 'reject') {
    return <Text>{error}</Text>
  }

  return (
    <Text>
      {location?.latitude} {location?.longitude}
    </Text>
  )
}
