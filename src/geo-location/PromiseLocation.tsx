import { Text } from '@chakra-ui/layout'
import React from 'react'

type Location = Readonly<{
  latitude: number
  longitude: number
}>

const getCurrentPosition = (): Promise<Location> =>
  new Promise((resolve, reject) => {
    navigator.geolocation.getCurrentPosition(
      pos =>
        resolve({
          longitude: pos.coords.longitude,
          latitude: pos.coords.latitude,
        }),
      err => reject(err.message),
    )
  })

export const Location = () => {
  const [location, setLocation] = React.useState<Location | undefined>(
    undefined,
  )
  const [error, setError] = React.useState<string | undefined>(undefined)

  React.useEffect(() => {
    getCurrentPosition().then(setLocation).catch(setError)
  }, [])

  return (
    <Text>
      {!error && !location && <h1>Loading...</h1>}
      {error && <h1>{error}</h1>}
      {location && <h1>{JSON.stringify(location, null, 2)}</h1>}
    </Text>
  )
}
