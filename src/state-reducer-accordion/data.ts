export interface Solider {
  readonly id: number
  readonly name: string
  readonly cost: number
  readonly kingdom: string
}

export const soliders: Solider[] = [
  { id: 1, name: 'ghsgd sdh', cost: 230, kingdom: 'KR' },
  { id: 2, name: 'jdbdfj fhd', cost: 647, kingdom: 'KK' },
  { id: 3, name: 'dhf djhf', cost: 283, kingdom: 'KL' },
  { id: 4, name: 'sdhf jhzd', cost: 300, kingdom: 'KP' },
  { id: 5, name: 'sdg jf', cost: 374, kingdom: 'KO' },
  // { id: 6, name: 'gzf jdf', cost: 837, kingdom: 'KS' },
  // { id: 7, name: 'dzjhf jhf', cost: 364, kingdom: 'KF' },
  // { id: 8, name: 'aksdj', cost: 384, kingdom: 'KB' },
]
