import { VStack, HStack, Image, Heading, Text, Box } from '@chakra-ui/react'
import React from 'react'
import { soliders, Solider } from './data'

interface SoliderViewProps {
  readonly soldier: Solider
  readonly toggle: boolean
}

export const SoliderView: React.FC<SoliderViewProps> = ({
  soldier,
  toggle,
}) => {
  return (
    <>
      {toggle ? (
        <VStack>
          <Heading fontSize="18px" fontStyle="italic">
            {soldier.name}
          </Heading>
          <Text as="li">buildcost:{soldier.cost}</Text>
          <Text as="li">Kingdom:{soldier.kingdom}</Text>
        </VStack>
      ) : null}
    </>
  )
}

interface AccordionListViewProps {
  readonly solider: Solider
  readonly toggle: boolean
  onAccordianClick(id: number): void
  onToggleClick(toggle: boolean): void
}

export const AccordionImageView: React.FC<AccordionListViewProps> = ({
  onAccordianClick,
  onToggleClick,
  toggle,
  solider,
}) => {
  return (
    <Box border="1px solid white">
      <Image
        src={`https://robohash.org/${solider.id}`}
        boxSize="100px"
        objectFit="cover"
        onClick={() => {
          onAccordianClick(solider.id)
          onToggleClick(toggle)
        }}
      />
    </Box>
  )
}

interface AccordianListViewProps {
  readonly list: readonly Solider[]
  readonly id: number
  onAccordianClick(id: number): void
}

export const AccordianListView: React.FC<AccordianListViewProps> = ({
  list,
  id,
  onAccordianClick,
}) => {
  return (
    <VStack>
      {list.map(sol => {
        const [toggle, setToggle] = React.useState(sol.id === id)
        return (
          <HStack key={sol.id}>
            <AccordionImageView
              toggle={toggle}
              solider={sol}
              onAccordianClick={onAccordianClick}
              onToggleClick={() => setToggle(!toggle)}
            />
            <SoliderView soldier={sol} toggle={toggle} />
          </HStack>
        )
      })}
    </VStack>
  )
}

export const AccordionReducer: React.FC = () => {
  const [id, set] = React.useState<number | undefined>(undefined)

  const handleAccordianClick = (id: number) => {
    set(id)
  }

  return (
    <AccordianListView
      list={soliders}
      id={id as number}
      onAccordianClick={handleAccordianClick}
    />
  )
}
