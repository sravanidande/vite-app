import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import { App } from './App'
import theme from './Theme'
import { ColorModeScript } from '@chakra-ui/react'
;(async function () {
  if (import.meta.env.DEV) {
    const { worker } = await import('./mocks/browser')
    await worker.start()
  }
})()
  .then(() => {
    ReactDOM.render(
      <>
        <ColorModeScript initialColorMode={theme.config.initialColorMode} />
        <App />
      </>,
      document.getElementById('root'),
    )
  })
  .catch(error => console.error(`msw err: ${JSON.stringify(error, null, 2)}`))

import.meta.hot?.accept()
