import { ChakraProvider, Container, Heading } from '@chakra-ui/react'
import React from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { AuthSupabaseContext } from './supabase-twitter/AuthContext'
import { Home } from './supabase-twitter/Home'
import { Navbar } from './supabase-twitter/Navbar'
import { SigninPage } from './supabase-twitter/SigninPage'
import { SignoutPage } from './supabase-twitter/SignoutPage'
// import { AccordionReducer } from './state-reducer-accordion/AccordionReducer'
import { SignupPage } from './supabase-twitter/SignupPage'
// import { ToggleSwitch } from './StateReducer'
// import { Location } from './geo-location/Location'
// import { ScrollableMain } from './scrollable/Scrollable.tssx'
// import { AnimatedCounter } from './accodian-components/AnimatedCounter'
// import { Rating } from './animated-counter/Rating'
// import { BigHeadAvatar } from './big-head-avatars/BigHeadAvatar'
// import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// import { PokemonNew } from './pokemons'
// import { CharacterNew } from './rick-morty-characters/CharacterView'
// import { ScrollableMain } from './scrollable/Scrollable'
import theme from './Theme'
// import { TiltView } from './vanilla-tilt/TiltView'
// import { AvatarUpdateForm } from './update-form/AvatarUpdateForm'

// const one = pokemons[4]

export const NoPokemon: React.FC = () => {
  return <Heading>No Pokemon Yet</Heading>
}

export const App = () => {
  return (
    <ChakraProvider theme={theme}>
      {/* <AccordionReducer /> */}
      <AuthSupabaseContext>
        <Router>
          <Navbar />
          <Container>
            <Switch>
              <Route path="/signup">
                <SignupPage />
              </Route>
              <Route path="/signin">
                <SigninPage />
              </Route>
              <Route path="/signout">
                <SignoutPage />
              </Route>
              <Route path="/home">
                <Home />
              </Route>
            </Switch>
          </Container>
        </Router>
      </AuthSupabaseContext>
    </ChakraProvider>
  )
}
