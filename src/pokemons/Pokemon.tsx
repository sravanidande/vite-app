import { Image } from '@chakra-ui/image'
import { Box, Flex, Heading } from '@chakra-ui/react'
import React from 'react'
import { useHistory, useParams } from 'react-router-dom'
import useSWR from 'swr'
import { PokemonPowerTable } from './PokemonPowerTable'
import { SearchPokemon } from './SearchPokemon'
import { Pokemon } from './type'

interface PokemonProps {
  readonly pokemon: Pokemon
}

export const PokemonView: React.FC<PokemonProps> = ({ pokemon }) => {
  return (
    <>
      <Flex alignItems="center" justify="center" h="100vh" direction="column">
        <Box border="1px solid white" p="10px 80px" borderRadius="10px">
          <Heading as="h3">
            {pokemon.name} ({pokemon.id})
          </Heading>
          <Box
            border="0.5px solid white"
            p={4}
            mt={4}
            mb={4}
            borderRadius="10px"
          >
            <Image
              boxSize="250px"
              objectFit="cover"
              src={`https://pokeres.bastionbot.org/images/pokemon/${parseInt(
                pokemon.id,
              )}.png`}
              alt="pokemon"
            />
          </Box>
          <PokemonPowerTable pokemon={pokemon} />
        </Box>
      </Flex>
    </>
  )
}

const fetcher = async (url: string) => {
  const res = await fetch(url)
  const result = await res.json()

  if (res.ok) {
    return result
  } else {
    throw result
  }
}

export const PokemonNew: React.FC = () => {
  const { name } = useParams<{ readonly name: string }>()

  const history = useHistory()

  const { data, error } = useSWR(
    name && `/pokemons/${name.toLowerCase()}`,
    fetcher,
  )

  const handlePokemon = (name: string) => {
    history.push(`/pokemons/${name.toLowerCase()}`)
  }

  if (error) return <div>{error.message}</div>
  if (name && !data) return <div>loading...</div>

  return (
    <Box>
      <SearchPokemon onClickPokemon={handlePokemon} />
      {name && <PokemonView pokemon={data} />}
    </Box>
  )
}
