import { VStack } from '@chakra-ui/layout'
import React from 'react'
import useSWR from 'swr'
import { PokemonView } from './Pokemon'
import { Pokemon } from './type'

interface PokemonListViewProps {
  readonly pokemons: readonly Pokemon[]
}

export const PokemonListView: React.FC<PokemonListViewProps> = ({
  pokemons,
}) => {
  return (
    <VStack>
      {pokemons.map(poke => (
        <PokemonView pokemon={poke} />
      ))}
    </VStack>
  )
}

const fetcher = (url: string) => fetch(url).then(r => r.json())

export const PokemonList: React.FC = () => {
  const { data, error } = useSWR('/pokemons', fetcher)
  if (error) return <div>failed to load</div>
  if (!data) return <div>loading...</div>
  return <PokemonListView pokemons={data} />
}
