interface Fast {
  readonly name: string
  readonly type: string
  readonly damage: number
}
interface Special {
  readonly name: string
  readonly type: string
  readonly damage: number
}

export interface Pokemon {
  readonly id: string
  readonly name: string
  readonly classification: string
  readonly types: readonly string[]
  readonly resistant: readonly string[]
  readonly weaknesses: readonly string[]
  readonly weight: Readonly<{ minimum: string; maximum: string }>
  readonly height: Readonly<{ minimum: string; maximum: string }>
  readonly fleeRate: number
  readonly evolutionRequirements?: Readonly<{ amount: number; name: string }>
  readonly evolutions?: readonly Readonly<{ id: number; name: string }>[]
  readonly 'Previous evolution(s)'?: readonly Readonly<{
    id: number
    name: string
  }>[]
  readonly maxCP: number
  readonly maxHP: number
  readonly attacks: Readonly<{
    fast: readonly Fast[]
    special: readonly Special[]
  }>
}
