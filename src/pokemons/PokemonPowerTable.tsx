import { Table, Thead, Th, Tbody } from '@chakra-ui/react'
import { Td, Tr } from '@chakra-ui/table'
import React from 'react'
import { Pokemon } from './type'

interface PokemonPowerTableProps {
  readonly pokemon: Pokemon
}

export const PokemonPowerTable: React.FC<PokemonPowerTableProps> = ({
  pokemon,
}) => {
  return (
    <Table size="sm" maxW="400px">
      <Thead>
        <Tr>
          <Th>Ability</Th>
          <Th>Type</Th>
          <Th>Damage</Th>
        </Tr>
      </Thead>
      <Tbody>
        {pokemon.attacks.special.map(poke => (
          <Tr key={poke.name}>
            <Td>{poke.name}</Td>
            <Td>{poke.type}</Td>
            <Td>{poke.damage}</Td>
          </Tr>
        ))}
      </Tbody>
    </Table>
  )
}
