import { Button } from '@chakra-ui/button'
import { Input } from '@chakra-ui/input'
import { Box, HStack } from '@chakra-ui/layout'
import React from 'react'

interface SearchPokemonProps {
  onClickPokemon(name: string): void
}

export const SearchPokemon: React.FC<SearchPokemonProps> = ({
  onClickPokemon,
}) => {
  const [pokemon, set] = React.useState('')
  return (
    <Box>
      <HStack maxW="400px" margin="0 auto">
        <Input
          placeholder="Enter pokemon"
          value={pokemon}
          onChange={evt => {
            set(evt.target.value)
          }}
        />
        <Button onClick={() => onClickPokemon(pokemon)}>Fetch</Button>
      </HStack>
    </Box>
  )
}
