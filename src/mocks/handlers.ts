import { rest } from 'msw'
import { pokemons } from '../pokemons'

export const handlers = [
  // rest.get('/pokemons', (_, res, ctx) => {
  //   return res(
  //     ctx.status(200),
  //     ctx.delay(Math.floor(Math.random() * 1000)),
  //     ctx.json(pokemons),
  //   )
  // }),
  rest.get('/pokemons/:name', (req, res, ctx) => {
    const { name } = req.params
    const result = pokemons.find(poke => poke.name.toLowerCase() === name)
    return res(
      ctx.status(200),
      ctx.delay(Math.floor(Math.random() * 1000)),
      ctx.json(result),
    )
  }),
]
