import { Box } from '@chakra-ui/layout'
import React from 'react'
import { AvatarCardView, Avatar } from './AvatarCard'
import { UserUpdateForm } from './UserUpdateForm'

// const avatar: Avatar = {
//   id: '451',
//   name: 'michiu',
//   biography: 'jzhd jdhfj zdkgfu',
// }

export const AvatarUpdateForm: React.FC = () => {
  const [avatar, setAvatar] = React.useState<undefined | Avatar>(undefined)
  const handleSubmit = (newAvatar: Avatar) => {
    setAvatar(newAvatar)
  }

  return (
    <Box maxW="400px" m="0 auto">
      <UserUpdateForm onSubmit={handleSubmit} />
      <AvatarCardView avatar={avatar} />
    </Box>
  )
}
