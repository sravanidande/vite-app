import { BigHead } from '@bigheads/core'
import { VStack, Heading, Text, HStack, Box } from '@chakra-ui/react'
import React from 'react'

export interface Avatar {
  readonly id: string
  readonly name: string
  readonly biography: string
}

interface AvatarCardViewProps {
  readonly avatar?: Avatar
}

export const AvatarCardView: React.FC<AvatarCardViewProps> = ({ avatar }) => {
  return (
    <HStack mt="20px" bgColor="#373c48" maxW="400px" borderRadius="10px" p="2">
      <Box h="100px" w="100px">
        <BigHead />
      </Box>
      <VStack>
        <Heading as="h6">{avatar ? avatar.name : 'UnNamed'}</Heading>
        <Text fontSize="xs">
          {avatar ? avatar.biography : 'No biography provided'}
        </Text>
        <Text fontSize="xs">{avatar?.id}</Text>
      </VStack>
    </HStack>
  )
}
