import {
  Box,
  Button,
  Center,
  FormControl,
  FormLabel,
  HStack,
  Input,
  Text,
  Textarea,
} from '@chakra-ui/react'
import React from 'react'
import { Avatar } from './AvatarCard'

interface UserUpdateFormProps {
  onSubmit(avatar: Avatar): void
}

export const UserUpdateForm: React.FC<UserUpdateFormProps> = ({ onSubmit }) => {
  const [userAvatar, setUserAvatar] = React.useState<Avatar>({
    id: '',
    name: '',
    biography: '',
  })

  return (
    <Box
      maxH="350px"
      maxW="400px"
      border="0.5px solid teal"
      p="25px"
      mt="40px"
      borderRadius="10px"
    >
      <form>
        <Center>
          <Text as="i" fontWeight="semibold" fontSize="18px">
            (USER UPDATE FORM)
          </Text>
        </Center>
        <HStack mt="10px">
          <FormControl id="id">
            <FormLabel>User ID</FormLabel>
            <Input
              placeholder="user ID"
              value={userAvatar?.id}
              name={userAvatar['id']}
              onChange={evt =>
                setUserAvatar({ ...userAvatar, id: evt.target.value })
              }
            />
          </FormControl>
          <FormControl id="first-name" isRequired>
            <FormLabel>First name</FormLabel>
            <Input
              placeholder="First name"
              value={userAvatar?.name}
              name={userAvatar['name']}
              onChange={evt =>
                setUserAvatar({ ...userAvatar, name: evt.target.value })
              }
            />
          </FormControl>
        </HStack>
        <FormControl id="biography">
          <FormLabel>Biography</FormLabel>
          <Textarea
            placeholder="Tell us more about yourself in less than 140 characters"
            value={userAvatar?.biography}
            name={userAvatar['biography']}
            onChange={evt =>
              setUserAvatar({ ...userAvatar, biography: evt.target.value })
            }
          />
        </FormControl>

        <HStack mt="5px">
          <Button variant="outline">Reset</Button>
          <FormControl>
            <Button
              variant="solid"
              color="teal"
              onClick={() => {
                onSubmit(userAvatar)
              }}
            >
              success
            </Button>
          </FormControl>
        </HStack>
      </form>
    </Box>
  )
}
