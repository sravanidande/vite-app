import {
  Box,
  Center,
  Heading,
  HStack,
  Image,
  Text,
  VStack,
} from '@chakra-ui/react'
import React from 'react'
import { Character } from './data'
import { MountSearchBar } from './MountSearchBar'
import { FetchCharacterById } from './SearchCharacterById'

interface CharacterViewProps {
  readonly character: Character
}

export const CharacterDetails: React.FC<CharacterViewProps> = ({
  character,
}) => {
  return (
    <VStack>
      <Heading as="h6">{character.name}</Heading>
      <Text fontSize="xs">{`#${character.id}, ${character.species}, ${character.status}, ${character.gender}`}</Text>
      <Text fontSize="xs">{`origin: ${character.origin.name}`}</Text>
      <Text fontSize="xs">{`location: ${character.location.name}`}</Text>
    </VStack>
  )
}

export const CharacterView: React.FC<CharacterViewProps> = ({ character }) => {
  return (
    <HStack spacing="6">
      <Image src={character.image} boxSize="100px" objectFit="cover" />
      <CharacterDetails character={character} />
    </HStack>
  )
}

interface FetchStatus {
  readonly data?: Character
  readonly isLoading: boolean
  readonly error?: string
}

const initialStatus: FetchStatus = {
  isLoading: true,
}

export const useFetch = (key?: string): FetchStatus => {
  const [state, setFetchChar] = React.useState(initialStatus)

  React.useEffect(() => {
    if (key === undefined) {
      return
    }
    fetch(key)
      .then(async res => {
        const result = await res.json()
        if (res.ok) return result
        throw result.error
      })
      .then(data => {
        setFetchChar({ data: data, isLoading: false })
      })
      .catch(err => {
        setFetchChar({ isLoading: false, error: err })
      })
  }, [key])

  return key === undefined ? { isLoading: false } : state
}

export const CharacterNew: React.FC = () => {
  const [charId, setCharId] = React.useState<number | undefined>(undefined)

  const { data, isLoading, error } = useFetch(
    charId ? `https://rickandmortyapi.com/api/character/${charId}` : undefined,
  )

  const [mounted, setMounted] = React.useState(true)

  if (isLoading) return <div>Loading...</div>

  if (error) return <div>{error}</div>

  const handleClickId = (id: number) => {
    setCharId(id)
  }

  return (
    <>
      <MountSearchBar
        mounted={mounted}
        onHandleClick={() => setMounted(!mounted)}
      />

      {mounted ? (
        <>
          <FetchCharacterById onClickId={handleClickId} />
          {data ? (
            <Box maxW="400" h="200" mt="50" border="1px solid teal">
              <CharacterView character={data} />
            </Box>
          ) : (
            <Box w="100" h="100" mt="50" border="1px solid teal">
              <Center>Whick Rick and Morty character</Center>
            </Box>
          )}
        </>
      ) : null}
    </>
  )
}
