import { Checkbox } from '@chakra-ui/react'
import React from 'react'

interface MountSearchBarProps {
  readonly mounted: boolean
  onHandleClick(mounted: boolean): void
}

export const MountSearchBar: React.FC<MountSearchBarProps> = ({
  mounted,
  onHandleClick,
}) => {
  return (
    <Checkbox isChecked={mounted} onChange={() => onHandleClick(mounted)}>
      Mount the Search Bar
    </Checkbox>
  )
}
