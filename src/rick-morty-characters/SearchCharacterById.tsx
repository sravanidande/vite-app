import { Button } from '@chakra-ui/button'
import { Input } from '@chakra-ui/input'
import { Box, HStack } from '@chakra-ui/layout'
import React from 'react'

interface FetchCharacterByIdProps {
  onClickId(id: number): void
}

export const FetchCharacterById: React.FC<FetchCharacterByIdProps> = ({
  onClickId,
}) => {
  const [char, set] = React.useState<string>('')
  const [disabled, setDisabled] = React.useState(true)
  return (
    <Box>
      <HStack maxW="400px" margin="0 auto">
        <Input
          placeholder="Pick a number"
          value={char}
          onChange={evt => {
            set(evt.target.value)
            setDisabled(false)
          }}
        />
        <Button disabled={disabled} onClick={() => char && onClickId(+char)}>
          Fetch
        </Button>
      </HStack>
    </Box>
  )
}
