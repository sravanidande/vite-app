import { Box, Flex } from '@chakra-ui/layout'
import React from 'react'
import { AnimatedCounterLeft } from './AnimatedCounterLeft'
import { AnimatedCounterProps } from './AnimatedCounterProps'
import { CircularProgressView } from './CircularProgress'
import { GiveHeart } from './GiveHeart'
import { Rating } from './Rating'

// const animatedProps: AnimatedCounterValues = {
//   min: 0,
//   max: 0,
//   step: 2,
//   current: 1,
// }

const min = 1
const max = 10
const step = 2

interface AnimatedCounterState {
  readonly current: number
  readonly events: number
}

const initial = { current: 1, events: 0 }

const getMotion = (num: number) => {
  if (num > 2 && num <= 7) {
    return 'default'
  } else if (num > 7) {
    return 'shake'
  } else {
    return 'entrance'
  }
}

export const AnimatedCounter: React.FC = () => {
  const [counterValue, setCounterValue] = React.useState<AnimatedCounterState>(
    initial,
  )

  const motion = getMotion(counterValue.current)

  const handleIncrement = (step: number) => {
    const { current, events } = counterValue

    const newValue = current + step

    if (newValue < max) {
      setCounterValue(counterValue => ({
        ...counterValue,
        current: newValue,
        events: events + 1,
      }))
    }
  }

  return (
    <Box>
      <Rating />
      <Flex justify="space-around" mt="100px">
        <AnimatedCounterLeft
          current={counterValue.current}
          step={step}
          motion={motion}
          onIncrement={handleIncrement}
          onRefresh={() =>
            setCounterValue(counterValue => ({
              ...counterValue,
              current: 1,
              events: 0,
            }))
          }
        />
        <AnimatedCounterProps
          min={min}
          max={max}
          step={step}
          current={counterValue.current}
          events={counterValue.events}
          motion={motion}
        />
        <GiveHeart />
        <CircularProgressView />
      </Flex>
    </Box>
  )
}
