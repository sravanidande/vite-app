import { Flex, Heading, HStack, IconButton } from '@chakra-ui/react'
import { motion as fMotion } from 'framer-motion'
import React from 'react'
import { BiPlusCircle, BiRefresh } from 'react-icons/bi'
// import { AnimatedCounterValues } from './AnimatedCounterProps'

interface AnimatedCounterLeftProps {
  readonly current: number
  readonly step: number
  readonly motion: 'shake' | 'entrance' | 'default'
  onIncrement(step: number): void
  onRefresh(): void
}

export const AnimatedCounterLeft: React.FC<AnimatedCounterLeftProps> = ({
  onIncrement,
  current,
  step,
  motion,
  onRefresh,
}) => {
  const animate =
    motion === 'entrance'
      ? { rotate: 360 }
      : motion === 'default'
      ? { scale: [2, 0.5, 1] }
      : { opacity: 0.5 }

  return (
    <Flex direction="column" alignItems="center">
      <fMotion.div
        animate={animate}
        transition={{ duration: 0.5, ease: 'easeInOut' }}
      >
        <Heading as="h1" size="4xl">
          {current}
        </Heading>
      </fMotion.div>
      <HStack mt="10px">
        <IconButton
          aria-label="refresh"
          icon={<BiRefresh />}
          onClick={onRefresh}
        />
        <IconButton
          aria-label="plus"
          icon={<BiPlusCircle />}
          onClick={() => onIncrement(step)}
        />
      </HStack>
    </Flex>
  )
}
