import { Icon, IconButton, Text, VStack } from '@chakra-ui/react'
import React from 'react'
import { FaHeart } from 'react-icons/fa'
import { AiFillCloseCircle } from 'react-icons/ai'
import { motion } from 'framer-motion'

export const GiveHeart: React.FC = () => {
  const [animate, setAnimate] = React.useState({
    x: 0,
    scale: [1, 2, 1],
    color: '#FFC0CB',
  })
  const [toggle, setToggle] = React.useState<boolean>(true)
  const [count, setCount] = React.useState(16)

  return (
    <VStack spacing={4}>
      <Text>{count}</Text>
      <motion.div
        animate={
          toggle ? animate : { scale: [1.5, 2, 1.5, 1], color: '#FFC0CB' }
        }
        whileHover={
          count >= 20
            ? { color: '#FF0000' }
            : {
                color: '#FFC0CB',
              }
        }
        onClick={() => {
          setToggle(!toggle)
          if (count < 20) {
            setCount(count => count + 1)
          }
          if (count >= 20) {
            setAnimate(animate => ({ ...animate, color: '#FF0000', x: 10 }))
          }
        }}
        transition={{
          duration: 0.5,
          repeat: 1,
          ease: 'easeOut',
        }}
      >
        <Icon aria-label="heart" as={FaHeart} fontSize="34px" />
      </motion.div>
      {count >= 1 ? (
        <IconButton
          aria-label="close"
          icon={<AiFillCloseCircle />}
          size="xs"
          onClick={() => setCount(0)}
        />
      ) : null}
    </VStack>
  )
}
