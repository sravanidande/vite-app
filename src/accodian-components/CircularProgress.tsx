import {
  CircularProgress,
  CircularProgressLabel,
  Flex,
  HStack,
  IconButton,
} from '@chakra-ui/react'
import { motion } from 'framer-motion'
import React from 'react'
import { BiPlusCircle, BiRefresh } from 'react-icons/bi'

export const CircularProgressView: React.FC = () => {
  const [progress, setProgress] = React.useState(65)
  // const [animate, setAnimate] = React.useState({ scale: [2, 1.5, 1] })
  const [toggle, setToggle] = React.useState(false)

  const handleProgressClick = () => {
    if (progress < 100) {
      setProgress(progress => progress + 5)
      setToggle(!toggle)
    } else {
      setProgress(0)
    }
  }

  const variants = {
    open: { scale: [1, 1.2, 1.4, 1.2, 1] },
    closed: { scale: [1, 1.15, 1.25, 1.15, 1] },
  }

  return (
    <Flex justify="space-between" direction="column">
      <motion.div
        animate={toggle ? 'open' : 'closed'}
        variants={variants}
        transition={{
          type: 'spring',
          duration: 0.3,
          stiffness: 50,
        }}
      >
        <CircularProgress value={progress} size="100px">
          <CircularProgressLabel onClick={handleProgressClick}>
            {progress}%
          </CircularProgressLabel>
        </CircularProgress>
      </motion.div>
      <HStack>
        <IconButton
          aria-label="refresh"
          icon={<BiRefresh />}
          onClick={() => setProgress(0)}
        />
        <IconButton
          aria-label="plus"
          icon={<BiPlusCircle />}
          onClick={handleProgressClick}
        />
      </HStack>
    </Flex>
  )
}
