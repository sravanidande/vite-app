import { IconButton } from '@chakra-ui/button'
import { Flex, HStack } from '@chakra-ui/layout'
import { range } from 'lodash'
import React from 'react'
import { FaRegStar, FaStar, FaWindowClose } from 'react-icons/fa'
import { motion } from 'framer-motion'

export const Rating: React.FC = () => {
  const [rating, setRating] = React.useState<number>(3)

  const handleRatingClick = () => {
    setRating(rating => rating + 1)
  }

  return (
    <Flex h="40px" justify="space-around">
      {rating < 5 ? (
        <IconButton
          aria-label="rating"
          icon={<FaRegStar />}
          onClick={handleRatingClick}
        />
      ) : (
        <IconButton
          aria-label="rating-exceeds"
          icon={<FaWindowClose />}
          onClick={() => setRating(0)}
        />
      )}
      <HStack spacing={3}>
        {range(rating).map((_, index) => (
          <motion.div
            key={index}
            animate={{ scale: [1, 2, 2, 1.2] }}
            transition={{ ease: 'easeOut', duration: 0.3 }}
          >
            <IconButton
              aria-label="rating"
              icon={<FaStar />}
              color="yellow.500"
            />
          </motion.div>
        ))}
      </HStack>
    </Flex>
  )
}
