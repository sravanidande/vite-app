import React from 'react'

export interface AnimatedCounterProps {
  readonly min: number
  readonly max: number
  readonly step: number
  readonly current: number
  readonly events: number
  readonly motion: 'entrance' | 'default' | 'shake'
}

const getPosition = (num: number) => {
  if (num > 2 && num <= 7) {
    return 'between'
  } else if (num > 7) {
    return 'end'
  } else {
    return 'start'
  }
}

export const AnimatedCounterProps: React.FC<AnimatedCounterProps> = ({
  current,
  events,
  max,
  min,
  step,
  motion,
}) => {
  return (
    <ul>
      <li>min:{min}</li>
      <li>max:{max}</li>
      <li>step:{step}</li>
      <li>current:{current}</li>
      <li>events:{events}</li>
      <li>position:{getPosition(current)}</li>
      <li>motion:{motion}</li>
    </ul>
  )
}
