import { Button } from '@chakra-ui/button'
import { Box,  VStack } from '@chakra-ui/react'
import React from 'react'
import { CharacterView } from '../rick-morty-characters/CharacterView'
import { Character } from '../rick-morty-characters/data'

interface ScrollableMainViewProps {
  readonly characters: readonly Character[]
}

export const ScrollableMainView: React.FC<ScrollableMainViewProps> = ({
  characters,
}) => {
  return (
    <VStack>
      {characters.map(character => (
        <CharacterView key={character.id} character={character} />
      ))}
    </VStack>
  )
}

export const ScrollableMain: React.FC = () => {
  const [data, setData] = React.useState([])
  const ref = React.useRef<HTMLDivElement | null>(null)

  React.useLayoutEffect(() => {
    scrollToBottom()
  }, [])

  const scrollToBottom = () => ref.current

  React.useEffect(() => {
    fetch('https://rickandmortyapi.com/api/character')
      .then(res => res.json())
      .then(data => setData(data.results))
      .catch(err => console.error(err))
  }, [])

  return (
    <VStack>
      <Button m="auto">Scroll Top</Button>
      <Box
        p="20px"
        maxW="450px"
        maxH="350px"
        overflow="auto"
        border="1px solid white"
        mt="20px"
        m="auto"
        ref={ref}
      >
        <ScrollableMainView characters={data} />
      </Box>
      <Button m="0 auto">Scroll Bottom</Button>
    </VStack>
  )
}
