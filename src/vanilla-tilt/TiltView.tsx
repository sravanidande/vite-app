import React from 'react'
import { Tilt } from './Tilt'

export const TiltView: React.FC = () => {
  return (
    <Tilt>
      <div className="totally-centered">vanilla-tilt.js</div>
    </Tilt>
  )
}
