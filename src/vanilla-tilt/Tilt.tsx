import React from 'react'
import VanillaTilt from 'vanilla-tilt'

export const Tilt: React.FC = () => {
  const ref = React.useRef<any>()
  React.useEffect(() => {
    const tiltNode = ref.current
    if (tiltNode === undefined) {
      return
    }
    VanillaTilt.init(tiltNode, {
      max: 25,
      speed: 400,
      glare: true,
      'max-glare': 0.5,
    })

    return () => tiltNode.vanillaTilt.destroy()
  })
  return (
    <div
      style={{
        width: '250px',
        height: '150px',
        position: 'relative',
        backgroundColor: 'red',
      }}
      ref={ref}
    >
      <div
        style={{
          width: '40%',
          height: '45%',
          position: 'absolute',
          top: '30%',
          left: '30%',
          boxShadow: '0 0 50px 0 rgba(51, 51, 51, 0.3)',
          backgroundColor: 'white',
        }}
      >
        <div style={{ zIndex: 10 }}>
          <p
            style={{
              display: 'flex',
              color: 'pink',
              alignItems: 'center',
              justifyContent: 'center',
              height: '70px',
            }}
          >
            Vanilla-tilt
          </p>
        </div>
      </div>
    </div>
  )
}
